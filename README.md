# Cloud Native Workshop
The objective of this workshop is to demonstrate how you can build a complete CI/CD pipeline from scratch on the Oracle Cloud Infrastructure (OCI) in less than 2 hours.

The pipeline that we propose to build is the following:

![Pipeline.png](pics/Pipeline.png)

The application that we want to create is a python web application that gets its data from a MySQL database. Both the application and the database will be deployed as docker containers.

There are 3 environments in the CI/CD pipeline:
- a development environment (DEV)
- a test environment (QA) where the application is dynamically deployed and tested after each commit
- a production environment (PROD) that stays up and running all the time

We use GitLab as our code repository and CI/CD tool, and Docker Hub to store our docker images.

We will provision the following components on the Oracle Cloud Infrastructure:
- a managed Kubernetes cluster (OKE)
- a development VM (oci-instance-1), also runnuing the Kubernetes console to access the OKE cluster
- a test VM (oci-instance-2) hosting a one-node Kubernetes cluster (MicroK8s) for the QA environment.
All these components are available from a regular or trial OCI account. We assume the you have such an account in the rest of the workshop.

The execution of the CI/CD pipeline is controlled by 2 GitLab runners that are installed on the 2 OCI instances.

# GitLab Setup
- If you do not have an account on GitLab already, create one
- Clone the https://gitlab.com/guillaume.goutaudier/cloudnative-workshop project into your own repository
- Register your ssh key and make sure you can use the git commands against your project

# Infrastructure setup using Terraform   
We will install the following components automatically with Terraform:
- OKE Cluster (Oracle-managed Kubernetes)
- OCI network and instances

Install terraform on your laptop following this guide:
https://community.oracle.com/docs/DOC-1019936

Download the `terraform/main.tf` file. Modify the `provider` section and `compartment_id` variable to match your OCI settings.

Then run the following command:
```
terraform init
terraform apply
```

Alternatively, you can first create the virtual machines and then the OKE cluster (which usually takes around 15min to be created):
```
terraform init
terraform apply -target=null_resource.configure-instances
terraform apply -target=oci_containerengine_node_pool.pool
```

For conveniance, you can store the SSH key from the Terraform output to ssh-key:
```
vi ssh-key
chmod 600 ssh-key
```

# OCI Dev VM setup (instance1)
*All commands in this sections should be executed from the 1st VM instance.*
### Clone the workshop repository from GitLab:
First generate an SSH key and save it in GilLab (User profile / Settings / SSH Keys):
```
cat .ssh/id_rsa.pub 
# Copy/paste to GitLab
git clone git@gitlab.com:guillaume.goutaudier/cloudnative-workshop.git
```
### Install Python dependencies
```
pip3 install -r cloudnative-workshop/app/requirements.txt 
```
### Install docker
Execute the `install_docker_ubuntu.sh`script from the `cloudnative-workshop/scripts` directory. This script runs the following commands:
```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER
```
### Install Kubectl
```
sudo snap install kubectl --classic
```

Install the Oracle Cloud Infrastructure CLI (instructions from https://docs.cloud.oracle.com/iaas/Content/ContEng/Tasks/contengdownloadkubeconfigfile.htm):
```
bash -c "$(curl -L https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh)"
oci setup config
cat ~/.oci/oci_api_key_public.pem
```
In the OCI console, go to your User Settings and add a new API key by copy/pasting the public key that we just generated.

From the OCI console go to **"Developer Services / Container Clusters (OKE)"**. Click on the cluster and then on "Access Kubeconfig". Generate the Kubectl config file by entering the commands that is displayed (step 2.).

Validate that you can connect to the cluster:
```
kubectl get nodes
```
### Install Gitlab Runner
Execute the `install_gitlab_runner.sh`script from the `cloudnative-workshop/scripts` directory. This script runs the following commands:
```
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner
sudo gitlab-runner uninstall
sudo gitlab-runner install --working-directory /home/ubuntu --user ubuntu
sudo systemctl restart gitlab-runner
```
### Register Gitlab Runner
In GitLab, go to "Settings / CI/CD / Runners" and write down your project registration token.
```
sudo gitlab-runner register
```
When asked for the token, enter the one you previously noted. 
When asked for the tags, enter `oke,docker`

# OCI Test VM setup (instance2)
*All commands in this sections should be executed from the 2d VM instance.*
### Clone the workshop repository from GitLab:
First generate an SSH key and save it in GilLab (User profile / Settings / SSH Keys):
```
cat .ssh/id_rsa.pub
# Copy/paste to GitLab
git clone git@gitlab.com:guillaume.goutaudier/cloudnative-workshop.git
```
### Install Python dependencies
```
pip3 install -r cloudnative-workshop/app/requirements.txt 
```
### Install MicroK8s (a local version of Kubernetes and Kubectl):
As the Kubernetes network layer relies on the local Linux firewall, we need to make some changes to the default image configuration. To do that, we can modify the /etc/iptables/rules.v4 file:
```
sudo vi /etc/iptables/rules.v4 
# Remove all the lines containing the REJECT keyword in the INPUT and FORWARD tables
sudo iptables-restore < /etc/iptables/rules.v4 
```
Then install the snap package:
```
sudo snap install microk8s --classic
sudo microk8s.enable dns dashboard
sudo snap alias microk8s.kubectl kubectl
sudo usermod -a -G microk8s ubuntu
kubectl get all
```

### Gitlab runner install
Execute the `install_gitlab_runner.sh` script from the `cloudnative-workshop/scripts` directory. This script runs the following commands:
```
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner
sudo gitlab-runner uninstall
sudo gitlab-runner install --working-directory /home/ubuntu --user ubuntu
sudo systemctl restart gitlab-runner
```
### Register Gitlab Runner
In GitLab, go to "Settings / CI/CD / Runners" and write down your project registration token.
```
sudo gitlab-runner register
```
When asked for the token, enter the one you previously noted.
When asked for the tags, enter `microk8s`.


# Develop the application on the Dev environment
*All commands in this sections should be executed from the 1st VM instance.*

Create a Docker volume and start the MySQL DB container:
```
docker volume create dbvol
docker run --rm -it -d -p 3306:3306 --name mysqldb -e MYSQL_ROOT_PASSWORD=secret -v dbvol:/var/lib/mysql mysql:latest
```
Check the status of the container and that you can connect to the database:
```
docker ps
mysql -u root -p --password=secret -h 127.0.0.1 -e "show databases;"
```
Initialize the DB:
```
cd cloudnative-workshop/app
export MYSQL_ROOT_PASSWORD=secret
export DB_SERVICE_SERVICE_HOST=127.0.0.1
export DB_SERVICE_SERVICE_PORT=3306
export MYSQL_MESSAGE="Hello world from Dev"
python3 init_db.py
```
Test the application:
```
export MYSQL_HOST=localhost
export MYSQL_ROOT_PASSWORD=secret
python3 app.py
curl localhost:5000
curl localhost:5000/cities.json
curl localhost:5000/db
```

Now that the application works well, build a Docker image from it, and upload the image to DockerHub:
```
docker build -t guillaumegoutaudier/cloud-native:app ./app
docker login -u guillaumegoutaudier -p uve6ahN2aeG1pah0
docker push guillaumegoutaudier/cloud-native:app
```

# Deploy the application in the Prod environment
*All commands in this sections should be executed from the 1st VM instance.*
### Initialise the database
Note the subnet OCID from the `terraform output` command and update the `cloudnative-workshop/oke/db.yml` file with it. Then create the DB container and associated service:
```
cd cloudnative-workshop/oke
kubectl create -f db.yml
```
Note the IP of the node on which the DB is running: 
```
kubectl describe service/db-service | grep "LoadBalancer Ingress:"
```
Initialize the database:
```
cd ../app
export MYSQL_ROOT_PASSWORD=secret
export DB_SERVICE_SERVICE_HOST=<IP noted in previous step>
export DB_SERVICE_SERVICE_PORT=3306
export MYSQL_MESSAGE='Hello world from production!'
python3 init_db.py
```

### Application micro-service on OKE
Deploy the application using the below command. This will create a deployment with 3 pods:
```
cd ../oke
kubectl create -f app_deployment.yml
```
Edit the `app_service.yml` file and replace the subnet OCID by the one that you created with Terraform (re-run `terraform output` if you need to display it again), then deploy the service:
```
kubectl create -f app_service.yml
```
Check in the OCI console that a Load Balancer has been automaticall created.
You can see the Load Balancer external IP using this command: 
```
kubectl describe service/app-service | grep "LoadBalancer Ingress:"
```

Test the application:
```
curl <IP_FROM_PREVIOUS_STEP>:5000
curl <IP_FROM_PREVIOUS_STEP>:5000/cities.json
curl <IP_FROM_PREVIOUS_STEP>:5000/db
```
# CI/CD pipeline
Now that everything is ready, we can run the CI/CD pipeline.

Make a modification to the application and commit your changes. This will trigger the execution of the pipeline. You can connect to GitLab to watch its execution. The steps of the pipeline are described in the `.gitlab-ci.yml` file. Below is an explanation of the main stages.

### Application Build
The application is built dynamically on oci-instance-1 as a new docker image after each commit.
It is then uploaded on Docker Hub, from where it can be pushed to the QA and PROD environments.

### QA deployment
The QA environment is fully hosted on MicroK8s (a Kubernetes local implementation) running on oci-instance-2.
2 micro-services are configured:
- the Database
- the Application
These micro-services are re-deployed at each build, and the database is re-initialized with a test dataset.

Some tests are then executed, and if all goes well the application is then updated in the PROD environment.

### PROD deployment
During the last stage of the CI/CD pipeline, the `deployment` is updated to use the latest build and automatically executes a rolling update (all pods are progressively replaced by new ones running the new version of the application). 

# Clean-up
Run the following command on oci-instance-1 and oci-instance-2:
```
kubectl delete service,pod,deployment --all
```
Destroy all the OCI resource with Terraform:
```
terraform destroy
```
Remove the API key from your User settings.

Remove the SSH keys and Runners from your Gitlab settings. 

# TODO
### Check why oci failed during demo
