sudo apt-get install -y python3-pip libaio1 unzip
pip3 install cx_Oracle --upgrade
sudo mkdir -p /opt/oracle
sudo cp app/instantclient-basiclite-linux.x64-18.3.0.0.0dbru.zip /opt/oracle/
cd /opt/oracle
sudo unzip instantclient-basiclite-linux.x64-18.3.0.0.0dbru.zip
echo $PWD/instantclient_18_3 > $HOME/oracle-instantclient.conf
sudo mv $HOME/oracle-instantclient.conf /etc/ld.so.conf.d/
sudo ldconfig
cd -

