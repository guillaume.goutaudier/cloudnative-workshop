#!/bin/bash
container=$(docker ps -q -f name=oracledb)
if [ ! $container == "" ]
then
	echo "Killing this container" ;
	echo $container ;
	docker stop $container ;
	sleep 5;
fi
