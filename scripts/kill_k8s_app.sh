#!/bin/bash

kubectl get service/app-service > /dev/null 2>&1
if [ $? -eq 0 ]; then 
  echo "Deleting app service..."
  kubectl delete service/app-service 
fi

kubectl get deployment/app-deployment > /dev/null 2>&1
if [ $? -eq 0 ]; then 
  echo "Deleting app deployment..."
  kubectl delete deployment/app-deployment
fi


