#!/bin/bash
echo -n "Waiting for application container to start..."
while ! kubectl describe deployment/app-deployment | grep MinimumReplicasAvailable ; 
do
       echo -n "."
       sleep 1
done   
echo "App deployment is now ready"
