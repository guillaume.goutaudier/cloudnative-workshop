import pynuodb

options = {"schema": "user"}
connect_kw_args = {'database': "test", 'host': "localhost", 'user': "dba", 'password': "dba", 'options': options}

connection = pynuodb.connect(**connect_kw_args)
cursor = connection.cursor()

cursor.execute("CREATE TABLE APPDATA(K VARCHAR (20) NOT NULL,V VARCHAR (100) NOT NULL,PRIMARY KEY (K))")
cursor.execute("INSERT INTO APPDATA (K,V) VALUES ('MESSAGE', 'HELLO WORLD FROM NUODB!')")
cursor.execute("COMMIT")
cursor.close()
connection.close()

