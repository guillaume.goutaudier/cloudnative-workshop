import json
from subprocess import Popen, PIPE

def get_service_external_ip(service_name):
    cmd = "kubectl get services/"+service_name+" -o json"
    p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    output = p.stdout.read()
    j = json.loads(output.strip().decode('utf-8'))
    return(j['status']['loadBalancer']['ingress'][0]['ip'])

if __name__ == '__main__':
    service_ip = get_service_external_ip("db-service")
    print("External IP:")
    print(service_ip)



