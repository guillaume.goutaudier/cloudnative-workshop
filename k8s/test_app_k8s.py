import os, urllib.request
from get_service_ip import get_service_ip

ip = get_service_ip('app-service')
url = 'http://'+ip+':5000'

print("Connecting to "+url)
response = urllib.request.urlopen(url)
data = response.read()

print("URL response:", data.decode('utf-8'))

