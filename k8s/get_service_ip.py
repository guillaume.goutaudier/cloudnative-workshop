import json
from subprocess import Popen, PIPE

def get_service_ip(service_name):
    cmd = "kubectl get services/"+service_name+" -o json"
    p = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    output = p.stdout.read()
    j = json.loads(output.strip().decode('utf-8'))
    return(j['spec']['clusterIP'])

if __name__ == '__main__':
    service_ip = get_service_ip("db-service")
    print(service_ip)


