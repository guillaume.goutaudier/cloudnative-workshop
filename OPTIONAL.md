# Optional - OKE Kubernetes Dashboard
To connect to the Kubernetes dashboard, use the following commands on yout local machine:
```
scp ubuntu@oci-instance-1:.kube/config ./kubectl-config
ssh -L 8001:127.0.0.1:8001 -o ServerAliveInterval=10 ubuntu@oci-instance-1
```

Once connected to oci-instance-1, simply start the proxy:
```
kubectl proxy
```

You can now connect to the dashboard on this URL:
```
http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
```

During the sign-in process, simply select the kubectl-config file.

# Optional - Connect the application to a Blockchain
A Blockchain network is configured on https://sdk.sub02181403080.vcn0218140308.oraclevcn.com:5003

Take a few minutes to connect to the dashboard and discover the Blockchain setup. We will enrich the application so that it will query the blockchain when it receivers a request on `/blockchain`

A full explanation of how Blockchain works is beyond the scope of this workshop. Here we will simply see the Blockchain as a decentrialized database that we can query using a REST interface:
```
curl -u customertenant@oracle.com:Welcome1 -H Content-type:application/json -X POST -d @query.json https://sdk.sub02181403080.vcn0218140308.oraclevcn.com:5018/restproxy1/bcsgw/rest/v1/transaction/query --insecure
```
The `blockchain_query.py` file has a `get_blockchain_data()` function that does the equivalent of the curl command. Modify the application to use this function:
```python
from blockchain_query import get_blockchain_data

@app.route('/blockchain')
def get_blockchain():
  return get_blockchain_data()
```
Commit the new code and watch the deployment of the new application through the CI/CD pipeline. 
Test the application using the `/blockchain` endpoint.
 

# Optional - Deploy a K8s application using Helm
Helm is a package manager for K8s. In this section we illustrate how it can eassily be used to deploy
a Cloud-Native application on OKE with a one-line command.
### Install Helm
Connect to oci-instance-1 and install the Helm package
```
sudo snap install helm --classic
helm init --history-max 200
helm init --upgrade
helm repo update
```
 
### Install MySQL
```
helm install --name mysql stable/mysql
```
Check all what has been deployed:
```
helm status mysql
```
In particular, you will note that a Persistent Volume Claim has been created automatically and linked to a new Block Volume.

Note the MySQL root password and connect to the MySQL Server:
```
kubectl get secret --namespace default mysql -o jsonpath="{.data.mysql-root-password}" | base64 --decode; echo
kubectl port-forward svc/mysql 3306:3306
mysql -h 127.0.0.1 -P 3306 -u root -p
```




