import os, mysql.connector
mysql_password = os.environ.get('MYSQL_ROOT_PASSWORD')
mysql_host = os.environ.get('DB_SERVICE_SERVICE_HOST')
mysql_port = os.environ.get('DB_SERVICE_SERVICE_PORT')
mysql_message = os.environ.get('MYSQL_MESSAGE')
connection = mysql.connector.connect(user='root', password=mysql_password, host=mysql_host, port=mysql_port)
cursor = connection.cursor()
cursor.execute("CREATE DATABASE IF NOT EXISTS appdb;")
cursor.execute("USE appdb;")
cursor.execute("DROP TABLE IF EXISTS appdata;")
cursor.execute("CREATE TABLE appdata ( mykey VARCHAR(20) NOT NULL, myvalue VARCHAR(100) NOT NULL, PRIMARY KEY (mykey) );")
insert_query = "INSERT INTO appdata (mykey, myvalue) VALUES ('message', '"+mysql_message+"');"
cursor.execute(insert_query)
cursor.execute("COMMIT;")
cursor.close()
connection.close()

