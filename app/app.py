import os, mysql.connector
from flask import Flask
from flask import Response
from flask import json
from blockchain_query import get_blockchain_data

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello World!"

@app.route('/cities.json')
def cities():
    data = {"cities" : ["Amsterdam","Berlin","New York","San Francisco","Tokyo"]}
    resp = Response(json.dumps(data), status=200, mimetype='application/json')
    return resp

@app.route('/db')
def get_message():
  mysql_password = os.environ.get('MYSQL_ROOT_PASSWORD')
  mysql_host = os.environ.get('DB_SERVICE_SERVICE_HOST')
  mysql_port = os.environ.get('DB_SERVICE_SERVICE_PORT')
  connection = mysql.connector.connect(user='root', password=mysql_password, host=mysql_host, port=mysql_port)
  cursor = connection.cursor()
  cursor.execute("USE appdb;")
  cursor.execute("SELECT myvalue FROM appdata WHERE mykey='message';")
  res = cursor.fetchone()[0]
  cursor.close()
  connection.close()
  return res

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)

