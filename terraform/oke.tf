# ------ Create a Kubernetes cluster (OKE)
resource "oci_containerengine_cluster" "oke" {
    compartment_id = "${var.compartment_id}"
    kubernetes_version = "v1.12.7"
    name = "${var.prefix}-oke"
    vcn_id = "${oci_core_virtual_network.vcn.id}"
}
resource "oci_containerengine_node_pool" "pool" {
    cluster_id = "${oci_containerengine_cluster.oke.id}"
    compartment_id = "${var.compartment_id}"
    kubernetes_version = "v1.12.7"
    name = "${var.prefix}-node-pool"
    node_image_name = "Oracle-Linux-7.6"
    node_shape = "VM.Standard2.1"
    subnet_ids = ["${oci_core_subnet.public-subnet1.id}"]
    quantity_per_subnet = "1"
}

