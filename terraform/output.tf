# ------ Show instances public IPs
output "instance1-public-ip" {
  value = "${oci_core_instance.instances.0.public_ip}"
}
output "instance2-public-ip" {
  value = "${oci_core_instance.instances.1.public_ip}"
}
output "subnet-id" {
  value = "${oci_core_subnet.public-subnet1.id}"
}
output "ssh-key" {
  value = "${tls_private_key.ssh_key.public_key_openssh}"
}
output "ssh-private-key" {
  value = "${tls_private_key.ssh_key.private_key_pem}"
}

