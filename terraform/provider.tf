# ------ Configure the Oracle Cloud Infrastructure provider with an API Key
# ------ https://docs.cloud.oracle.com/iaas/Content/API/SDKDocs/terraformgetstarted.htm
provider "oci" {
  tenancy_ocid = "ocid1.tenancy.oc1..aaaaaaaapzsvi6joxlefvrtcoigx7zongyiwzo3bccykakfqksbqqhjpkeaq"
  user_ocid = "ocid1.user.oc1..aaaaaaaadjt7ifmwdytytfiil2o3okpabhmkq4beoinfwkx6gloujvaz6wha"
  fingerprint = "9f:d8:a8:4e:da:09:d9:9e:0e:5f:c5:ec:3b:2e:f3:a9"
  private_key_path = "/Users/goutaudi/.oci/oci_api_key.pem"
  #region = "eu-frankfurt-1"
  region = "us-ashburn-1"
}

# ------ Manually set compartment ID and prefix for the components created during the workshop
variable "compartment_id" { default = "ocid1.compartment.oc1..aaaaaaaafjqtlroamfbvixfelcr66rs4yf6xbhostwe44tgv7ehtatbpibfq" }
variable "prefix" { default = "cloudnative-workshop" }

# ------ Get a list of Availability Domains
data "oci_identity_availability_domains" "ads" {
  compartment_id = "${var.compartment_id}"
}

# ------ Get Ubuntu Images
data "oci_core_images" "ubuntu-images" {
  compartment_id = "${var.compartment_id}"
  operating_system = "Canonical Ubuntu"
  operating_system_version = "18.04"
  sort_by = "TIMECREATED"
  sort_order = "DESC"
}



