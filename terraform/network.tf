# ------ Create a new VCN
variable "VCN-CIDR" { default = "10.5.0.0/16" }
resource "oci_core_virtual_network" "vcn" {
  cidr_block = "${var.VCN-CIDR}"
  compartment_id = "${var.compartment_id}"
  display_name = "${var.prefix}-vcn"
  dns_label = "tfdemovcn"
}

# ------ Create a new Internet Gateway
resource "oci_core_internet_gateway" "ig" {
  compartment_id = "${var.compartment_id}"
  display_name = "${var.prefix}-internet-gateway"
  vcn_id = "${oci_core_virtual_network.vcn.id}"
}

# ------ Create a new Route Table
resource "oci_core_route_table" "rt" {
  compartment_id = "${var.compartment_id}"
  vcn_id = "${oci_core_virtual_network.vcn.id}"
  display_name = "${var.prefix}-route-table"
  route_rules {
    destination = "0.0.0.0/0"
    network_entity_id = "${oci_core_internet_gateway.ig.id}"
  }
}

# ------ Create a new security list to be used in the new subnet
resource "oci_core_security_list" "sl" {
  compartment_id = "${var.compartment_id}"
  display_name = "${var.prefix}-subnet1-security-list"
  vcn_id = "${oci_core_virtual_network.vcn.id}"
  egress_security_rules {
    protocol = "all"
    destination = "0.0.0.0/0"
  }
  ingress_security_rules {
    protocol = "all"
    source = "0.0.0.0/0"
  }
}

# ------ Create a public subnet 1 in AD1 in the new VCN
resource "oci_core_subnet" "public-subnet1" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ads.availability_domains[0],"name")}"
  cidr_block = "10.5.1.0/24"
  display_name = "${var.prefix}-public-subnet1"
  dns_label = "subnet1"
  compartment_id = "${var.compartment_id}"
  vcn_id = "${oci_core_virtual_network.vcn.id}"
  route_table_id = "${oci_core_route_table.rt.id}"
  security_list_ids = ["${oci_core_security_list.sl.id}"]
  dhcp_options_id = "${oci_core_virtual_network.vcn.default_dhcp_options_id}"
}

